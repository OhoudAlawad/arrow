
$(window).scroll(function(){
    if($(this).scrollTop()>=200){
        $("#navBar").addClass("noTransparrent");
     }
    else{
        $("#navBar").removeClass("noTransparrent");
     }
});


$(document).ready(function(){
    
    

    // to animate when travelling across the page
    $("a.scroll").on('click', function(event){
        event.preventDefault();
        var hash = this.hash;
        $('html, body').animate({scrollTop: $(hash).offset().top},1200,function(){});
    });
    
    
});

